#include "Mesh.h"

Mesh::Mesh(Vertex* vertices, size_t numVerts, uint32_t* indices, size_t numIndices) {
	myIndexCount = numIndices;
	myVertexCount = numVerts;


	// Create and bind our vertex array
	glCreateVertexArrays(1, &myVao);
	glBindVertexArray(myVao);
	// Create 2 buffers, 1 for vertices and the other for indices
	glCreateBuffers(2, myBuffers);

	// Bind and buffer our vertex data
	glBindBuffer(GL_ARRAY_BUFFER, myBuffers[0]); //Verticies
	glBufferData(GL_ARRAY_BUFFER, numVerts * sizeof(Vertex), vertices, GL_STATIC_DRAW);
	// Bind and buffer our index data
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, myBuffers[1]); //Indicies	
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, numIndices * sizeof(uint32_t), indices, GL_STATIC_DRAW);

	// Get a null vertex to get member offsets from
	Vertex* vert = nullptr;

	// Enable vertex attribute 0
	glEnableVertexAttribArray(0);
	// Our first attribute is 3 floats, the distance between
	// them is the size of our vertex, and they will map to the position in our vertices
	glVertexAttribPointer(0, 3, GL_FLOAT, false, sizeof(Vertex), &(vert->Position));
	// Enable vertex attribute 1
	glEnableVertexAttribArray(1);
	// Our second attribute is 4 floats, the distance between
	// them is the size of our vertex, and they will map to the color in our vertices
	glVertexAttribPointer(1, 4, GL_FLOAT, false, sizeof(Vertex), &(vert->Normal));

	// Unbind our VAO
	glBindVertexArray(0);

}

Mesh::~Mesh() {
	// Clean up our buffers
	glDeleteBuffers(2, myBuffers);
	// Clean up our VAO
	glDeleteVertexArrays(1, &myVao);
}

void Mesh::Draw() {
	// Bind the mesh
	glBindVertexArray(myVao);
	// Draw all of our vertices as triangles, our indexes are unsigned ints (uint32_t)
	glDrawElements(GL_TRIANGLES, myIndexCount, GL_UNSIGNED_INT, nullptr);
}

void Mesh::loadObject(const char* f, std::vector<Vertex>& vertices, unsigned int& num_verts, std::vector<unsigned int>& indices, unsigned int& num_indices)
{
	std::ifstream file;
	file.open(f, std::ios::in);

	std::vector<glm::vec3> positions = std::vector<glm::vec3>();
	std::vector<glm::vec3> normals;
	std::vector<glm::vec2> textures;

	std::string line;

	while (!file.eof()) {

		if (file.peek() == '#') {
			file.ignore(512, (int)'\n');
			continue;
		}

		file >> line;

		if (line.compare("v") == 0) {
			glm::vec3 temp;

			file >> line;
			temp.x = std::stof(line);
			file >> line;
			temp.y = std::stof(line);
			file >> line;
			temp.z = std::stof(line);

			positions.push_back(temp);

		}
		else if (line.compare("vn") == 0) {
			glm::vec3 temp;

			file >> line;
			temp.x = std::stof(line);
			file >> line;
			temp.y = std::stof(line);
			file >> line;
			temp.z = std::stof(line);

			normals.push_back(temp);

		}
		else if (line.compare("vt") == 0) {
			glm::vec2 temp;

			file >> line;
			temp.x = std::stof(line);
			file >> line;
			temp.y = std::stof(line);


			textures.push_back(temp);

		}
		else if (line.compare("f") == 0) {

			int count = 0;
			int base = vertices.size();

			glm::vec3 temp;

			while (file.peek() != '\n') {
				file >> line;

				int p, t, n;

				std::istringstream data(line);
				std::string i;

				//Must be done in Position >> Texture >> Normal because OBJ files

				std::getline(data, i, '/');
				p = std::stoi(i);

				std::getline(data, i, '/');
				t = std::stoi(i);

				std::getline(data, i, '/');
				n = std::stoi(i);

				Vertex temp;
				temp.Position = positions[p - 1];
				temp.Texture = textures[t - 1];
				temp.Normal = normals[n - 1];

				vertices.push_back(Vertex(temp));
				count++;
			}

			if (count == 3) {
				indices.push_back(base);
				indices.push_back(base + 1);
				indices.push_back(base + 2);
			}
			else if (count == 4) {
				indices.push_back(base);
				indices.push_back(base + 1);
				indices.push_back(base + 2);

				indices.push_back(base + 3);
				indices.push_back(base + 2);
				indices.push_back(base);
			}
		}
		else
		{
			file.ignore(512, (int)'\n');
		}
	}
	num_verts = vertices.size();
	num_indices = indices.size();
}

