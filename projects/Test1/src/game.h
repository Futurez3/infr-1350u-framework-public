
#pragma once
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include "GLM/glm.hpp"

#include "Mesh.h"
#include "Shader.h"
#include "Camera.h"

class Game {
public:
	float PerspectiveCD = 0.0f;

	Game();
	~Game();
	void Run();
protected:
	void Initialize();
	void Shutdown();
	void LoadContent();
	void UnloadContent();
	void InitImGui();
	void ShutdownImGui();
	void ImGuiNewFrame();
	void ImGuiEndFrame();
	void Update(float deltaTime);
	void Draw(float deltaTime);
	void DrawGui(float deltaTime);
private:
	// Stores the main window that the game is running in
	GLFWwindow* myWindow;
	// Stores the clear color of the game's window
	glm::vec4 myClearColor;
	// Stores the title of the game's window
	char myWindowTitle[32];

	bool ortho = false;
	bool buttonClicked = false;

	Camera::Sptr myCamera;

	glm::mat4 myModelTransform;
	glm::mat4 myNewTransform;
	glm::mat4 TriangleTransform;

	glm::mat4 myNewTranslate;

	Mesh::Sptr myMesh;
	Mesh::Sptr testMesh;

	Shader::Sptr myShader;
	Shader::Sptr testShader;
};