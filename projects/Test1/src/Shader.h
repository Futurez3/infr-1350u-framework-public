#pragma once
#include <glad/glad.h>
#include <memory>
#include <GLM/glm.hpp>

class Shader {
public:
	typedef std::shared_ptr<Shader> Sptr;

	Shader();
	~Shader();

	void Compile(const char* vs_source, const char* fs_source);

	//Loads a shader program from 2 files, VS and FS files.

	void Load(const char* vsFile, const char* fsFile);

	void SetUniform(const char* name, const glm::mat4& value);

	void Bind();

private:
	GLuint __CompileShaderPart(const char* source, GLenum type);

	GLuint myShaderHandle;


};
