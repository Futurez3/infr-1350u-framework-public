#include "game.h"
#include <stdexcept>
#include <vector>

#define IMGUI_IMPL_OPENGL_LOADER_GLAD
#include "imgui.h"
#include "imgui_impl_opengl3.h"
#include "imgui_impl_glfw.h"
#include "Logging.h"

#include <GLM/gtc/matrix_transform.hpp>

Game::Game() :
	myWindow(nullptr),
	myWindowTitle("Game"),
	myClearColor(glm::vec4(0.1f, 0.7f, 0.5f, 1.0f))
{

}

//destructors
Game::~Game() {}

void GlfwWindowResizedCallback(GLFWwindow* window, int width, int height) {
	glViewport(0, 0, width, height);
}


void Game::Initialize() {
	// Initialize GLFW
	if (glfwInit() == GLFW_FALSE) {
		LOG_ASSERT(false,"Failed to initialize GLFW")
	}

	// Enable transparent backbuffers for our windows (note that Windows expects our colors to be pre-multiplied with alpha)
	glfwWindowHint(GLFW_TRANSPARENT_FRAMEBUFFER, true);

	// Create a new GLFW window
	myWindow = glfwCreateWindow(600, 600, myWindowTitle, nullptr, nullptr);

	// We want GL commands to be executed for our window, so we make our window's context the current one
	glfwMakeContextCurrent(myWindow);

	// Let glad know what function loader we are using (will call gl commands via glfw)
	LOG_ASSERT(gladLoadGLLoader((GLADloadproc)glfwGetProcAddress) != 0, "Failed to load GLAD") {
		
	}

	// Tie our game to our window, so we can access it via callbacks
	glfwSetWindowUserPointer(myWindow, this);
	// Set our window resized callback
	glfwSetWindowSizeCallback(myWindow, GlfwWindowResizedCallback);

}

//Shutdown
void Game::Shutdown() {
	glfwTerminate();
}

void Game::LoadContent() {

	myCamera = std::make_shared<Camera>();
	myCamera->SetPosition(glm::vec3(5, 5, 5));
	myCamera->LookAt(glm::vec3(0));
	myCamera->Projection = glm::perspective(glm::radians(60.0f), 1.0f, 0.01f, 1000.0f);

	myModelTransform = glm::mat4(1.0f);
	myNewTransform = glm::mat4(1.0f);
	myNewTransform = glm::translate(myNewTransform, glm::vec3(0, 3, 0));
	

	//TriangleTransform = glm::mat4(1.0f);

	// Create our 4 vertices

	unsigned int num_vertices;
	unsigned int num_indices;

	std::vector<Vertex> v;
	std::vector<unsigned int> i;

	Mesh::loadObject("OBJ_Load_Toast.obj", v, num_vertices, i, num_indices);

	Vertex* vertices;
	unsigned int* indices;

	vertices = new Vertex[num_vertices];
	indices = new unsigned int[num_indices];

	for (int counter = 0; counter < v.size(); counter++) {
		vertices[counter] = v[counter];
	}

	for (int counter = 0; counter < i.size(); counter++) {
		indices[counter] = i[counter];
	}

	// Create a new mesh from the data

	myMesh = std::make_shared<Mesh>(vertices, num_vertices, indices, num_indices);

	

	//testMesh = std::make_shared<Mesh>(vertices_tri, 3, indices_tri, 3);


	// Create and compile shader
	myShader = std::make_shared<Shader>();
	myShader->Load("passthrough.vert.glsl", "passthrough.frag.glsl");

	testShader = std::make_shared<Shader>();
	testShader->Load("passthrough.vert.glsl", "passthrough.frag.glsl");


}
void Game::UnloadContent() {

}
void Game::Update(float deltaTime) {
	
	

	glm::vec3 movement = glm::vec3(0.0f);
	glm::vec3 rotation = glm::vec3(0.0f);
	float speed = 2.0f;
	float rotSpeed = 12.5f;



	// Rotate our transformation matrix a little bit each frame
	myModelTransform = glm::rotate(myModelTransform, deltaTime, glm::vec3(0, 0, 1));
	myNewTransform = glm::rotate(myNewTransform, deltaTime, glm::vec3(0, 0, -1));
	

	

	if (glfwGetKey(myWindow, GLFW_KEY_W) == GLFW_PRESS)
		movement.z -= speed * deltaTime;
	if (glfwGetKey(myWindow, GLFW_KEY_S) == GLFW_PRESS)
		movement.z += speed * deltaTime;
	if (glfwGetKey(myWindow, GLFW_KEY_A) == GLFW_PRESS)
		movement.x -= speed * deltaTime;
	if (glfwGetKey(myWindow, GLFW_KEY_D) == GLFW_PRESS)
		movement.x += speed * deltaTime;
	if (glfwGetKey(myWindow, GLFW_KEY_Z) == GLFW_PRESS)
		movement.y += speed * deltaTime;
	if (glfwGetKey(myWindow, GLFW_KEY_X) == GLFW_PRESS)
		movement.y -= speed * deltaTime;



	if (glfwGetKey(myWindow, GLFW_KEY_Q) == GLFW_PRESS)
		rotation.z -= rotSpeed * deltaTime;
	if (glfwGetKey(myWindow, GLFW_KEY_E) == GLFW_PRESS)
		rotation.z += rotSpeed * deltaTime;
	if (glfwGetKey(myWindow, GLFW_KEY_UP) == GLFW_PRESS)
		rotation.x -= rotSpeed * deltaTime;
	if (glfwGetKey(myWindow, GLFW_KEY_DOWN) == GLFW_PRESS)
		rotation.x += rotSpeed * deltaTime;
	if (glfwGetKey(myWindow, GLFW_KEY_LEFT) == GLFW_PRESS)
		rotation.y -= rotSpeed * deltaTime;
	if (glfwGetKey(myWindow, GLFW_KEY_RIGHT) == GLFW_PRESS)
		rotation.y += rotSpeed * deltaTime;

//	if (glfwGetKey(myWindow, GLFW_KEY_Z) == GLFW_PRESS) {
//		TriangleTransform = glm::translate(TriangleTransform, glm::vec3(0, 0, 2) * deltaTime);
//	}
//	if (glfwGetKey(myWindow, GLFW_KEY_X) == GLFW_PRESS) {
//		TriangleTransform = glm::translate(TriangleTransform, glm::vec3(0, 0, -2) * deltaTime);
//	}

	if (glfwGetKey(myWindow, GLFW_KEY_SPACE) == GLFW_PRESS && PerspectiveCD == 0) {


		if (ortho == false) {
			myCamera->Projection = glm::ortho(-2.0f, 2.0f, -2.0f, 2.0f,0.01f,1000.0f);
			ortho = true;
		}
		else {
			myCamera->Projection = glm::perspective(glm::radians(60.0f), 1.0f, 0.01f, 1000.0f);
			ortho = false;
		}
		
		PerspectiveCD = 2.0f;
	}

	// Rotate and move our camera based on input
	myCamera->Rotate(rotation);
	myCamera->Move(movement);

	PerspectiveCD -= deltaTime;

	if (PerspectiveCD < 0.0f)
		PerspectiveCD = 0.0f;
}


/// Initiating IMGui

void Game::InitImGui() {
	// Creates a new ImGUI context
	ImGui::CreateContext();

	// Gets our ImGUI input/output
	ImGuiIO& io = ImGui::GetIO();

	// Enable keyboard navigation
	io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;

	// Allow docking to our window
	io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;

	// Allow multiple viewports (so we can drag ImGui off our window)
	io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;

	// Allow our viewports to use transparent backbuffers
	io.ConfigFlags |= ImGuiConfigFlags_TransparentBackbuffers;

	// Set up the ImGui implementation for OpenGL
	ImGui_ImplGlfw_InitForOpenGL(myWindow, true);

	ImGui_ImplOpenGL3_Init("#version 410");
	// Dark mode FTW
	ImGui::StyleColorsDark();

	// Get our imgui style
	ImGuiStyle& style = ImGui::GetStyle();

	//style.Alpha = 1.0f;
	if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable) {
		style.WindowRounding = 0.0f;
		style.Colors[ImGuiCol_WindowBg].w = 0.8f;
	}
}

//ImGui Cleanup
void Game::ShutdownImGui() {
	// Cleanup the ImGui implementation
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	// Destroy our ImGui context
	ImGui::DestroyContext();
}


//Internal Housekeeping
void Game::ImGuiNewFrame() {
	// Implementation new frame
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	// ImGui context new frame
	ImGui::NewFrame();
}

void Game::ImGuiEndFrame() {
	// Make sure ImGui knows how big our window is
	ImGuiIO& io = ImGui::GetIO();
	int width{ 0 }, height{ 0 };
	glfwGetWindowSize(myWindow, &width, &height);
	io.DisplaySize = ImVec2(width, height);
	// Render all of our ImGui elements
	ImGui::Render();
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
	// If we have multiple viewports enabled (can drag into a new window)
	if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable) {
		// Update the windows that ImGui is using
		ImGui::UpdatePlatformWindows();
		ImGui::RenderPlatformWindowsDefault();
		// Restore our gl context
		glfwMakeContextCurrent(myWindow);
	}
}

void Game::Draw(float deltaTime) {
	// Clear our screen every frame
	glClearColor(myClearColor.x, myClearColor.y, myClearColor.z, myClearColor.w);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glEnable(GL_DEPTH_TEST);

	myShader->Bind();
	// Update our uniform
	myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * myModelTransform);
	myMesh->Draw();

	
	myShader->Bind();
	myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * myNewTransform);
	myMesh->Draw();

	//testShader->Bind();
	//myShader->SetUniform("a_ModelViewProjection", myCamera->GetViewProjection() * TriangleTransform);
	//testMesh->Draw();
}

void Game::DrawGui(float deltaTime) {
	// Open a new ImGui window
	ImGui::Begin("Test");
	// Draw widgets here

	// Draw a color editor
	ImGui::ColorEdit4("Clear Color", &myClearColor[0]);
	if (ImGui::InputText("Window Title", myWindowTitle, 32)) {
		glfwSetWindowTitle(myWindow, myWindowTitle);
	}


	ImGui::End();
}



void Game::Run()
{
	Initialize();
	InitImGui();
	LoadContent();
	static float prevFrame = glfwGetTime();
	// Run as long as the window is open
	while (!glfwWindowShouldClose(myWindow)) {
		// Poll for events from windows
		// clicks, key presses, closing, all that
		glfwPollEvents();
		float thisFrame = glfwGetTime();
		float deltaTime = thisFrame - prevFrame;
		Update(deltaTime);
		Draw(deltaTime);
		ImGuiNewFrame();
		DrawGui(deltaTime);
		ImGuiEndFrame();
		prevFrame = thisFrame;
		// Present our image to windows
		glfwSwapBuffers(myWindow);

		prevFrame = thisFrame;
	}
	UnloadContent();
	ShutdownImGui();
	Shutdown();

}
