#pragma once

#include <glad/glad.h>
#include <GLM/glm.hpp> // For vec3 and vec4
#include <cstdint> // Needed for uint32_t
#include <memory> // Needed for smart pointers

#include <string>
#include <fstream>
#include <sstream>
#include <vector>


struct Vertex {
	glm::vec3 Position;
	//glm::vec4 Color;
	glm::vec3 Normal;
	glm::vec2 Texture;
};

class Mesh {
public:
	// Shorthand for shared_ptr
	typedef std::shared_ptr<Mesh> Sptr;
	// Creates a new mesh from the given vertices and indices
	Mesh(Vertex* vertices, size_t numVerts, uint32_t* indices, size_t numIndices);
	~Mesh();
	// Draws this mesh
	void Draw();
	static void loadObject(const char* f, std::vector<Vertex>& verts, unsigned int& num_verts, std::vector<unsigned int>& indices, unsigned int& num_indi);
private:
	// Our GL handle for the Vertex Array Object
	GLuint myVao;

	// 0 is vertices, 1 is indices
	GLuint myBuffers[2];

	// The number of vertices and indices in this mesh
	size_t myVertexCount, myIndexCount;
};

